import 'package:cinetpay_social_app/ContactWidget.dart';
import 'package:cinetpay_social_app/models/ContactResult.dart';
import 'package:cinetpay_social_app/network/BackendService.dart';
import 'package:flutter/material.dart';

import 'models/User.dart';

class ContactScreen extends StatefulWidget {

  final User user;
  const ContactScreen({this.user});


  @override
  _ContactScreenState createState() => _ContactScreenState();

}

class _ContactScreenState extends State<ContactScreen> {

  bool _loading = true;

  List<User> contacts = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    fetchContacts();

  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();

  }

  @override
  Widget build(BuildContext context) {

    if(contacts == null){
      contacts = [];
    }

    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Contacts"),
      ),
      body: new Stack(
        children: [
          contacts.isEmpty? emptyView() : new SingleChildScrollView(
            child: new Column(
              children: contacts.map((e) => new ContactWidget(e,(){

                Navigator.pop(context, e);

              })).toList(),
            ),
          ),

          new Offstage(
            offstage: _loading,
            child: progressWidget(),
          )
        ],
      ),
    );
  }


  Widget progressWidget(){
    return new Stack(
      children: [

        Container(
          color: Colors.white,
        ),

        Center(child: CircularProgressIndicator())
      ],
    ) ;
  }

  void showProgress(){

    if(mounted){
      setState(() {
        _loading = false;
      });
    }


  }

  void hideProgress(){

    if(mounted){
      setState(() {
        _loading = true;
      });
    }
  }

  Widget emptyView() {

    return new Container(
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          new Center(
            child: new Text("Aucune donnée  disponible", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30.0),),
          ),
          new Center(
            child: new Text("Revenez plutard dans l'espoir d'avoir quelque chose pour vous.", style: TextStyle( fontSize: 16.0),),
          )
        ],
      ),
    );
  }

  void fetchContacts() {

    BackendService.findContacts(
      id: widget.user.id,
      showDialog: showProgress,
      hideDialog: hideProgress
    ).then((ContactResult value){

      if(value != null && value.success && value.data != null && mounted ){

        setState(() {

          contacts = value.data;

        });

      }


    }).catchError((onError){

      print("Error found $onError");

    });

  }


}
