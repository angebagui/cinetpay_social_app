import 'dart:convert';

import 'package:cinetpay_social_app/database/DatabaseHelper.dart';
import 'package:cinetpay_social_app/models/User.dart';

class LoginManager{


  static Future logOut() async{
    await DatabaseHelper.deleteUsers();
  }


  static Future storeUser(User user) async{
    await DatabaseHelper.deleteUsers();
    await DatabaseHelper.insertUser({
      "data":jsonEncode(user.toJson())
    });
  }

  static Future<User> connectedUser() async{
    final List<Map<String, dynamic>> result  =  await DatabaseHelper.allUsers(1);
    if(result.isNotEmpty){
      final Map<String, dynamic> row = result.first;
      final String data = row['data'] as String;
      final Map<String, dynamic> userJson = jsonDecode(data);
      final User user = User.fromJson(userJson);
      return user;
    }else{
      return null;
    }
  }

}