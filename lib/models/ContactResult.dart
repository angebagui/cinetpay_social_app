import 'User.dart';

class ContactResult{

  final bool success;
  final List<User> data;
  final String message;

  const ContactResult({this.success, this.data, this.message});

  static ContactResult fromJSON(Map<dynamic, dynamic> json){

    return new ContactResult(
      success:  json['success'] as bool ,
      data: (json['data'] as List).map((element) => User.fromJSON(element as Map)).toList(),
      //  data: json['data'] as Map,
      message: json['message'] as String

    );
  }

}