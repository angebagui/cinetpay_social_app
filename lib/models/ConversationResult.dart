import 'package:json_annotation/json_annotation.dart';

import 'Conversation.dart';


part 'ConversationResult.g.dart';

@JsonSerializable()
class ConversationResult{

  final bool success;
  final Conversation data;
  final String message;

  const ConversationResult({this.success, this.data, this.message});


  factory ConversationResult.fromJson(Map<String, dynamic> json) => _$ConversationResultFromJson(json);
  Map<String, dynamic> toJson() => _$ConversationResultToJson(this);


}