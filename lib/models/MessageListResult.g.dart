// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MessageListResult.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MessageListResult _$MessageListResultFromJson(Map<String, dynamic> json) {
  return MessageListResult(
    success: json['success'] as bool,
    data: (json['data'] as List)
        ?.map((e) =>
            e == null ? null : Message.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$MessageListResultToJson(MessageListResult instance) =>
    <String, dynamic>{
      'success': instance.success,
      'data': instance.data,
      'message': instance.message,
    };
