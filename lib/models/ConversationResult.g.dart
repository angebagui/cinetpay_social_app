// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ConversationResult.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ConversationResult _$ConversationResultFromJson(Map<String, dynamic> json) {
  return ConversationResult(
    success: json['success'] as bool,
    data: json['data'] == null
        ? null
        : Conversation.fromJson(json['data'] as Map<String, dynamic>),
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$ConversationResultToJson(ConversationResult instance) =>
    <String, dynamic>{
      'success': instance.success,
      'data': instance.data,
      'message': instance.message,
    };
