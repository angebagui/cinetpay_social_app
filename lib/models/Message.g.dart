// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Message.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Message _$MessageFromJson(Map<String, dynamic> json) {
  return Message(
    id: json['id'] as int,
    content: json['content'] as String,
    content_type: json['content_type'] as String,
    sender_id: json['sender_id'] as int,
    conversation_id: json['conversation_id'] as int,
    is_read: json['is_read'] as bool,
    is_received: json['is_received'] as bool,
    is_sent: json['is_sent'] as bool,
    sender: json['sender'] == null
        ? null
        : User.fromJson(json['sender'] as Map<String, dynamic>),
    created_at: json['created_at'] as String,
    updated_at: json['updated_at'] as String,
  );
}

Map<String, dynamic> _$MessageToJson(Message instance) => <String, dynamic>{
      'id': instance.id,
      'content': instance.content,
      'content_type': instance.content_type,
      'sender_id': instance.sender_id,
      'conversation_id': instance.conversation_id,
      'is_read': instance.is_read,
      'is_received': instance.is_received,
      'is_sent': instance.is_sent,
      'sender': instance.sender,
      'created_at': instance.created_at,
      'updated_at': instance.updated_at,
    };
