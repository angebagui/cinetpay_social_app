import 'package:json_annotation/json_annotation.dart';

import 'Conversation.dart';


part 'ConversationListResult.g.dart';

@JsonSerializable()
class ConversationListResult{

  final bool success;
  final List<Conversation> data;
  final String message;

  const ConversationListResult({this.success, this.data, this.message});


  factory ConversationListResult.fromJson(Map<String, dynamic> json) => _$ConversationListResultFromJson(json);
  Map<String, dynamic> toJson() => _$ConversationListResultToJson(this);


}