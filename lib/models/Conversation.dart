import 'package:json_annotation/json_annotation.dart';

import 'Message.dart';
import 'User.dart';

part 'Conversation.g.dart';

@JsonSerializable()
class Conversation{

  final int id;
  final List<int> speakers;
  final bool is_group;
  final String group_name;
  final dynamic admins;
  final List<int> speaker_list;
  final List<User> users;
  final List<Message> messages;
  final String created_at;
  final String updated_at;

  const Conversation(
  {this.id,
    this.speakers,
    this.is_group,
    this.group_name,
    this.admins,
    this.speaker_list,
    this.users,
    this.messages,
    this.created_at,
    this.updated_at});

  factory Conversation.fromJson(Map<String, dynamic> json) => _$ConversationFromJson(json);
  Map<String, dynamic> toJson() => _$ConversationToJson(this);

  User getReceiver(int connectedUserId) {

    if(users == null){
      return null;
    }

    if(users.isEmpty){
      return null;
    }
    return users.singleWhere((element) => element.id != connectedUserId, orElse: null);
  }

  Message lastMessage(){

    if(messages == null){
      return null;
    }

    if(messages.isEmpty){
      return null;
    }

    return messages.first;
  }

}