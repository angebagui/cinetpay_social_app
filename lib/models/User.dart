import 'package:json_annotation/json_annotation.dart';

part 'User.g.dart';

@JsonSerializable(nullable: false)
class User{

  final int id;
  final String first_name;
  final String last_name;
  final String email;
  final String password;
  final String photo;
  final String created_at;
  final String updated_at;

  const User({
    this.id, this.first_name, this.last_name, this.email, this.password, this.photo,
    this.created_at, this.updated_at
});

  static User fromJSON(Map<dynamic, dynamic> json){
    return new User(
      id: json['id'] as int,
      first_name: json['first_name'] as String,
      last_name: json['last_name'] as String,
      email: json['email'] as String,
      password: json['password'] as String,
      photo: json['photo'] as String,
      created_at: json['created_at'] as String,
      updated_at: json['updated_at'] as String
    );
  }

  DateTime createdAt(){
    return created_at == null? null: DateTime.parse(created_at);
  }

  DateTime updatedAt(){
    return updated_at == null? null: DateTime.parse(updated_at);
  }

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
  
}