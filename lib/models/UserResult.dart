import 'User.dart';

class UserResult{

  final bool success;
  final User data;
  final String message;

  const UserResult({this.success, this.data, this.message});

  static UserResult fromJSON(Map<dynamic, dynamic> json){

    return new UserResult(
      success:  json['success'] as bool ,
      data: User.fromJSON(json['data'] as Map),
      //  data: json['data'] as Map,
      message: json['message'] as String

    );
  }

}