// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MessageResult.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MessageResult _$MessageResultFromJson(Map<String, dynamic> json) {
  return MessageResult(
    success: json['success'] as bool,
    data: json['data'] == null
        ? null
        : Message.fromJson(json['data'] as Map<String, dynamic>),
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$MessageResultToJson(MessageResult instance) =>
    <String, dynamic>{
      'success': instance.success,
      'data': instance.data,
      'message': instance.message,
    };
