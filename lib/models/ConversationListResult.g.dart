// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ConversationListResult.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ConversationListResult _$ConversationListResultFromJson(
    Map<String, dynamic> json) {
  return ConversationListResult(
    success: json['success'] as bool,
    data: (json['data'] as List)
        ?.map((e) =>
            e == null ? null : Conversation.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$ConversationListResultToJson(
        ConversationListResult instance) =>
    <String, dynamic>{
      'success': instance.success,
      'data': instance.data,
      'message': instance.message,
    };
