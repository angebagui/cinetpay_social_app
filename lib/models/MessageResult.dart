import 'package:json_annotation/json_annotation.dart';

import 'Message.dart';


part 'MessageResult.g.dart';

@JsonSerializable()
class MessageResult{

  final bool success;
  final Message data;
  final String message;

  const MessageResult({this.success, this.data, this.message});


  factory MessageResult.fromJson(Map<String, dynamic> json) => _$MessageResultFromJson(json);
  Map<String, dynamic> toJson() => _$MessageResultToJson(this);


}