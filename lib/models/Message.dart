import 'package:json_annotation/json_annotation.dart';

import 'User.dart';

part 'Message.g.dart';

const TYPE_TEXT = "text";
const TYPE_AUDIO = "audio";
const TYPE_IMAGE = "image";
const TYPE_VIDEO = "video";

@JsonSerializable()
class Message{

  static final String TYPE_TEXT = "text";
  static final String TYPE_AUDIO = "audio";
  static final String TYPE_IMAGE = "image";
  static final String  TYPE_VIDEO = "video";

  final int id;
  final String content;
  final String content_type;
  final int sender_id;
  final int conversation_id;
  final bool is_read;
  final bool is_received;
  final bool is_sent;
  final User sender;
  final String created_at;
  final String updated_at;

  const Message({
    this.id,
    this.content,
    this.content_type,
    this.sender_id,
    this.conversation_id,
    this.is_read,
    this.is_received,
    this.is_sent,
    this.sender,
    this.created_at,
    this.updated_at
  });

  factory Message.fromJson(Map<String, dynamic> json) => _$MessageFromJson(json);
  Map<String, dynamic> toJson() => _$MessageToJson(this);

  DateTime createdAt(){
    return DateTime.parse(created_at);
  }
}