// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Conversation.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Conversation _$ConversationFromJson(Map<String, dynamic> json) {
  return Conversation(
    id: json['id'] as int,
    speakers: (json['speakers'] as List)?.map((e) => e as int)?.toList(),
    is_group: json['is_group'] as bool,
    group_name: json['group_name'] as String,
    admins: json['admins'],
    speaker_list:
        (json['speaker_list'] as List)?.map((e) => e as int)?.toList(),
    users: (json['users'] as List)
        ?.map(
            (e) => e == null ? null : User.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    messages: (json['messages'] as List)
        ?.map((e) =>
            e == null ? null : Message.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    created_at: json['created_at'] as String,
    updated_at: json['updated_at'] as String,
  );
}

Map<String, dynamic> _$ConversationToJson(Conversation instance) =>
    <String, dynamic>{
      'id': instance.id,
      'speakers': instance.speakers,
      'is_group': instance.is_group,
      'group_name': instance.group_name,
      'admins': instance.admins,
      'speaker_list': instance.speaker_list,
      'users': instance.users,
      'messages': instance.messages,
      'created_at': instance.created_at,
      'updated_at': instance.updated_at,
    };
