import 'package:json_annotation/json_annotation.dart';

import 'Message.dart';


part 'MessageListResult.g.dart';

@JsonSerializable()
class MessageListResult{

  final bool success;
  final List<Message> data;
  final String message;

  const MessageListResult({this.success, this.data, this.message});


  factory MessageListResult.fromJson(Map<String, dynamic> json) => _$MessageListResultFromJson(json);
  Map<String, dynamic> toJson() => _$MessageListResultToJson(this);


}