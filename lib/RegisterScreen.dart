import 'dart:convert';
import 'dart:io';

import 'package:cinetpay_social_app/HomeScreen.dart';
import 'package:cinetpay_social_app/LoginScreen.dart';
import 'package:cinetpay_social_app/models/UserResult.dart';
import 'package:cinetpay_social_app/network/BackendService.dart';
import 'package:cinetpay_social_app/util/LoginManager.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:toast/toast.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {

  bool _isTermAccepted = false;

  TextEditingController firstNameController = new TextEditingController();
  TextEditingController lastNameController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  bool _hide = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    firstNameController.addListener(() {

      print('On CHANGE >>> ${firstNameController.text.toString()}');
    });
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();

  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Stack(
        children: [
          new SingleChildScrollView(
            child: new Column(
              children: [

                new Center(
                    child: new Container(
                      width: 150.0,
                      height: 150.0,
                      margin: EdgeInsets.only(top: 50.0),
                      padding: EdgeInsets.all(16.0),
                      decoration: BoxDecoration(
                        //color: Colors.black,
                        border: Border.all(color: Theme.of(context).primaryColor),
                        shape: BoxShape.circle,
                        //image: DecorationImage(image: NetworkImage("https://cinetpay.com/images/logo.png"))

                      ),
                      child: Image.network("https://cinetpay.com/images/logo.png", ),
                    )),

                new SizedBox(height: 10.0,),
                new Center(
                  child: new Text("Créer compte", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30.0),),
                ),
                new SizedBox(height: 10.0,),
                new Container(
                  child: new Text("Veuillez remplir ces champs:"),
                ),

                new Container(
                  padding: EdgeInsets.symmetric(horizontal: 16.0),
                  margin: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 8.0),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12.0),
                      border: Border.all(color: Theme.of(context).primaryColor)
                  ),
                  child: new TextField(
                    controller: firstNameController,
                    decoration: InputDecoration(
                      hintText: "Prénoms",
                      border: InputBorder.none,
                    ),
                    keyboardType: TextInputType.name,
                  ),
                ),

                new Container(
                  padding: EdgeInsets.symmetric(horizontal: 16.0),
                  margin: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 8.0),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12.0),
                      border: Border.all(color: Theme.of(context).primaryColor)
                  ),
                  child: new TextField(
                    controller: lastNameController,
                    decoration: InputDecoration(
                        hintText: "Nom",
                        border: InputBorder.none
                    ),
                    keyboardType: TextInputType.name,
                  ),
                ),
                new Container(
                  padding: EdgeInsets.symmetric(horizontal: 16.0),
                  margin: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 8.0),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12.0),
                      border: Border.all(color: Theme.of(context).primaryColor)
                  ),
                  child: new TextField(
                    controller: emailController,
                    decoration: InputDecoration(
                        hintText: "Email",
                        border: InputBorder.none
                    ),
                    keyboardType: TextInputType.emailAddress,
                  ),
                ),

                new Container(
                  padding: EdgeInsets.symmetric(horizontal: 16.0),
                  margin: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12.0),
                      border: Border.all(color: Theme.of(context).primaryColor)
                  ),
                  child: new TextField(
                    controller: passwordController,
                    obscureText: true,
                    decoration: InputDecoration(
                        hintText: "Mot de passe",
                        border: InputBorder.none
                    ),
                  ),
                ),


                new Container(
                    margin: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
                    child: InkWell(
                        onTap: (){

                          print('Mot de passe Oublié clicked');
                        },
                        child: new Row(
                          children: [
                            new Checkbox(
                              value: _isTermAccepted,
                              onChanged: (bool value){

                                if(mounted){
                                  setState(() {

                                    _isTermAccepted = value;

                                  });
                                }


                              },
                            ),
                            new Expanded(
                              child:  new Text("J'accepte les conditions d'utilisation pour rejoindre CinetPay Social",
                                style: TextStyle(decoration: TextDecoration.underline),),
                            )
                          ],
                        )
                    )
                ),

                new Container(
                  width: double.infinity,
                  height: 50.0,
                  margin: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
                  child: new RaisedButton(
                    onPressed: createUser,
                    color: Theme.of(context).accentColor,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                    child: new Text("S'inscrire", style: TextStyle(color: Colors.white),),
                  ),
                ),


                new Container(
                  width: double.infinity,
                  height: 50.0,
                  margin: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
                  child: new RaisedButton(
                    onPressed: (){

                      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> LoginScreen()));
                    },
                    color: Theme.of(context).primaryColor,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                    child: new Text("Se Connecter", style: TextStyle(color: Colors.white),),
                  ),
                ),



              ],
            ),
          ),
          new Offstage(
            offstage: _hide,
            child:progressWidget(),
          )
        ],
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(RegisterScreen oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);

  }

  void createUser() {

    String firstName = firstNameController.text.trim().toString();
    String lastName = lastNameController.text.trim().toString();
    String email = emailController.text.trim().toString();
    String password = passwordController.text.trim().toString();

    print("FIRSTNAME $firstName");
    print("LASTTNAME ${lastNameController.text.trim().toString()}");
    print("EMAIL $email");
    print("PASSWORD $password");

    if(firstNameController.text.trim().toString().isEmpty){

      //Display Toast
      Toast.show("Prénoms obligatoire", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);

      return;
    }

    if(lastNameController.text.trim().toString().isEmpty){

      //Display Toast
      Toast.show("Nom obligatoire", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      return;
    }

    if(emailController.text.trim().toString().isEmpty){

      //Display Toast
      Toast.show("Email obligatoire", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      return;
    }

    if(passwordController.text.trim().toString().isEmpty){

      //Display Toast
      Toast.show("Mot de passe obligatoire", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      return;
    }

    if(!_isTermAccepted){
      Toast.show("Vous devez accepter les conditions d'utilisations !", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      return;
    }




    BackendService.register(
        firstName: firstNameController.text.trim().toString(),
        lastName: lastNameController.text.trim().toString(),
        email: emailController.text.trim().toString(),
        password: passwordController.text.trim().toString(),
        showDialog: showProgress,
      hideDialog: hideProgress
    )
    .then((UserResult value){

      print("VALUE $value");


      if(value.success){

        LoginManager.storeUser(value.data)
            .catchError((onError){
              print("Error $onError");
            })
            .whenComplete((){

          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context)=> HomeScreen(user:value.data)));

        });
      }else{
        Toast.show(value.message, context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      }

    })
    .catchError((dynamic onError){

      print("Error found $onError");

      if(onError is SocketException){
        Toast.show("Vérifiez votre connexion internet !", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      }else if(onError is Response){

        if(onError.statusCode == 400){

          final Map result = jsonDecode(onError.body);
          String message = "Nous rencontrons des problèmes avec le serveur";
          if(result != null && result.containsKey("message")){

            message = result['message'] as String;
          }

          Toast.show(message, context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
        }

      } else{
        final String message = "Nous rencontrons des problèmes avec le serveur";
        Toast.show(message, context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      }

    });


  }

  Widget progressWidget(){
   return new Stack(
     children: [

       Container(
           color: Colors.white,
       ),

       Center(child: CircularProgressIndicator())
     ],
   ) ;
  }

  void showProgress(){

    _hide = false;

   if(mounted){
     setState(() {
       _hide = false;
     });
   }


  }

  void hideProgress(){

    if(mounted){
      setState(() {
        _hide = true;
      });
    }
  }
}
