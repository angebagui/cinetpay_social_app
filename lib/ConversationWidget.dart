import 'package:cinetpay_social_app/models/Conversation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'ConversationCinetPay.dart';
import 'models/User.dart';

class ConversationWidget extends StatelessWidget {
  
  final Conversation element;
  final User connectedUser;
  final Function onTap;
  const ConversationWidget({this.element, this.connectedUser, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: new ConversationCinetPay(
          leftWidget: new Container(
            width: 60.0,
            height: 60.0,
            child: new Stack(
              children: [

                new Container(
                  width: 60.0,
                  height: 60.0,
                  decoration: BoxDecoration(
                      color: Colors.black,
                      shape: BoxShape.circle,
                      image: DecorationImage(image: NetworkImage(getPhotoUrl()))
                  ) ,
                ),

                Positioned.fill(
                  top: 40.0,
                  right: 40.0,
                  child: new Container(
                    width: 18.0,
                    height: 18.0,
                    decoration: BoxDecoration(
                        color: Color(0xFF00F86B),
                        shape: BoxShape.circle,
                        border:Border.all(color: Colors.white,width: 4.0)
                    ) ,
                  ),
                )


              ],
            ),
          ),
          titleWidget: new Text(getTitle()),
          subtitleWidget: new Text(getLastMessageContent()),
          rightWidget: new Container(
              child: new Column(
                children: [
                  new Text(getLastMessageDate(), style: TextStyle(color: Color(0xFF9F9F9F)),),
                  new SizedBox(height: 10.0,),
                  Offstage(
                    offstage: true,
                    child: new Container(
                      padding: EdgeInsets.only(left:12.0, right: 12.0, top: 6.0, bottom: 6.0),
                      decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(20.0)
                      ),
                      child: new Text("14", style: TextStyle(color: Colors.white),),
                    ),
                  )

                ],
              )
          )
      ),
    );
  }

  String getTitle() {

    if(element== null){
      return "Nothing";
    }else{

      if(element.is_group != null && element.is_group){
        return element.group_name;
      }else{
        final User receiver =  element.getReceiver(connectedUser.id);
        return receiver.first_name+" "+receiver.last_name;
      }

    }


  }

  String getPhotoUrl() {

    if(element== null){
      return "https://www.shareicon.net/data/512x512/2016/01/09/700702_network_512x512.png";
    }else{

      if(element.is_group != null && element.is_group){
        return "https://www.shareicon.net/data/512x512/2016/01/09/700702_network_512x512.png";
      }else{
        final User receiver =  element.getReceiver(connectedUser.id);
        return receiver.photo;
      }

    }
  }

  String getLastMessageContent() {

    if(element.lastMessage() != null){
      return element.lastMessage().content;
    }else{
      return "";
    }

  }

  String getLastMessageDate() {
    if(element.lastMessage() != null){
      return "${DateFormat.Hm().format(element.lastMessage().createdAt())}";
    }else{
      return "";
    }

  }
}
