import 'dart:convert';
import 'dart:io';

import 'package:cinetpay_social_app/models/Conversation.dart';
import 'package:cinetpay_social_app/models/ConversationResult.dart';
import 'package:cinetpay_social_app/models/Message.dart';
import 'package:cinetpay_social_app/models/MessageListResult.dart';
import 'package:cinetpay_social_app/models/MessageResult.dart';
import 'package:cinetpay_social_app/network/BackendService.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:toast/toast.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import 'models/User.dart';

class InboxScreen extends StatefulWidget {

  final User receiver;
  final User connectedUser;
  final Conversation conversation;
  const InboxScreen({ @required this.connectedUser, this.receiver, this.conversation});

  @override
  _InboxScreenState createState() => _InboxScreenState();

}

class _InboxScreenState extends State<InboxScreen> {

  Conversation _conversation;
  TextEditingController messageController = new TextEditingController();
  List<Message>  _messages = [];

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

   if(mounted){
     setState(() {
       _conversation =  widget.conversation;
     });
   }

    print("RECEIVER USER_ID => ${widget.receiver.id}");
    print("CONNECTED USER_ID => ${widget.connectedUser.id}");

    messageController.addListener(() {

      if(messageController.text.isNotEmpty){

        // TODO PUSH to say that we are typing....
      }

    });

    findOrCreateConversation();

  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");

        loadMessages();

      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");

      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");

      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    _firebaseMessaging.getToken().then((String token) {

    });

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: new Row(
          children: [
              new Container(
              width: 40.0,
              height: 40.0,
              decoration: BoxDecoration(

                  shape: BoxShape.circle,
                  image: DecorationImage(image: NetworkImage(getPhotoUrl()))
              ),
            ),
            new SizedBox(width: 5.0,),
            new Expanded(
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    new Text("${getTitle()}", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18.0),),
                    new SizedBox(height: 5.0,),
                    new Text("Online", style: TextStyle(color: Colors.black, fontSize: 16.0),),
                    new SizedBox(height: 5.0,),
                  ],
                )
            )
          ],
        )
      ),
      body: new Column(
        children: [
          new SizedBox(height: 20.0,),
          _buildMessageListUi(),
          new Divider(thickness: 1.0,),
          new Container(
            height: 70.0,
            width: MediaQuery.of(context).size.width,
            color: Colors.white,
            child: new Row(
              children: [
                new SizedBox(width: 10.0,),
                new IconButton(
                    icon: new Icon(Icons.add,color: Colors.black,size: 35.0,),
                    onPressed: (){

                }),
                new SizedBox(width: 10.0,),
                new Expanded(
                  child: new TextField(
                    controller: messageController,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Write a message"
                    ),
                  ),
                ),
                new SizedBox(width: 10.0,),
                new IconButton(
                    icon: new Icon(Icons.send,color: Colors.black,size: 35.0,),
                    onPressed: (){

                      sendMessage();

                    }),
                new SizedBox(width: 10.0,),

              ],
            ),
          )
        ],
      ),
    );
  }

  String getTitle() {

    if(_conversation == null){
      return widget.receiver.first_name+" "+widget.receiver.last_name;
    }else{

      if(_conversation.is_group != null && _conversation.is_group){
        return _conversation.group_name;
      }else{
        final User receiver =  _conversation.getReceiver(widget.connectedUser.id);
        return receiver.first_name+" "+receiver.last_name;
      }

    }


  }

  String getPhotoUrl() {

    if(_conversation == null){
      return widget.receiver.photo;
    }else{

      if(_conversation.is_group != null && _conversation.is_group){
        return "https://www.shareicon.net/data/512x512/2016/01/09/700702_network_512x512.png";
      }else{
        final User receiver =  _conversation.getReceiver(widget.connectedUser.id);
        return receiver.photo;
      }

    }
  }

  Widget _buildDateWidget(){
    return new Container(
      child: new Text("Today", style: TextStyle(color: Colors.grey[500],),),
      alignment: Alignment.center,
      width: double.infinity,
      padding: EdgeInsets.only(left:16.0, right: 16.0, top: 8.0, bottom: 8.0),
      margin: EdgeInsets.fromLTRB(60.0, 20.0, 60.0, 20.0),
      decoration: BoxDecoration(
        color: Colors.grey[100],
        borderRadius: BorderRadius.circular(6.0)
      ),
    );
  }
  Widget _buildMessageListUi(){
    // _buildDateWidget(),


    if(_messages == null){
      _messages = [];
    }
    /**
     *
     *  [


        _buildIncomingMessageUi(),
        _buildOutcomingMessageUi(),
        _buildIncomingMessageUi(),
        _buildOutcomingMessageUi(),
        _buildIncomingMessageUi(),
        _buildOutcomingMessageUi(),
        _buildIncomingMessageUi(),
        _buildOutcomingMessageUi()
        ]
     *
     * **/
    return new Expanded(
      child: _messages.isEmpty ? emptyView() : new SingleChildScrollView(
        child: new Column(
          children: _messages.map((Message e) => e.sender_id == widget.connectedUser.id ? _buildOutcomingMessageUi(e):_buildIncomingMessageUi(e)).toList(),
        ),
      ),
    );
  }

  Widget _buildOutcomingMessageUi(Message element){

    return new Container(
      margin: EdgeInsets.only(bottom: 16.0),
      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [


          new SizedBox(width: 70.0,),
          new Expanded(
            child: new Container(
              child: new Stack(
                alignment: Alignment.bottomRight,
                children: [


                   element.content_type == Message.TYPE_TEXT ?new Container(
                child: new Text(element.content == null?"":element.content,textAlign: TextAlign.left, style: TextStyle(color: Colors.white,),),
                alignment: Alignment.topLeft,
                padding: EdgeInsets.fromLTRB(18.0, 18.0, 40.0,18.0),
                decoration: BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10.0),
                      bottomRight: Radius.circular(10.0),
                      bottomLeft:  Radius.circular(10.0),
                    )
                ),
              ) : _buildOutcomingMediaUi(element),
                  new Container(
                    padding: EdgeInsets.all(4.0),
                    child: new Text(/*"12:12AM"*/"${DateFormat.Hm().format(element.createdAt())}" , style: TextStyle(color: Colors.white, fontSize: 10.0),),
                  ),
                ],
              ),
            ),
          ),
          new SizedBox(width: 16.0,),

        ],
      ),
    );
  }

  Widget _buildIncomingMessageUi(Message element){

    return new Container(
      margin: EdgeInsets.only(bottom: 16.0),
      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          new SizedBox(width: 16.0,),
          new Container(
            width: 40.0,
            height: 40.0,
            decoration: BoxDecoration(
                color: Colors.black,
                shape: BoxShape.circle,
              image: DecorationImage(
                image: NetworkImage(element.sender.photo)
              )
            ),
          ),
          new SizedBox(width: 10.0,),
          new Expanded(
            child: new Container(
              child: new Stack(
                alignment: Alignment.bottomRight,
                children: [

                  element.content_type == Message.TYPE_TEXT ?new Container(
                    child: new Text(element.content == null?"":element.content,textAlign: TextAlign.left, style: TextStyle(color: Colors.black,),),
                    alignment: Alignment.topLeft,
                    padding: EdgeInsets.all(18.0),
                    decoration: BoxDecoration(
                        color: Color(0xFFE2E2E4),
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(10.0),
                          bottomRight: Radius.circular(10.0),
                          bottomLeft:  Radius.circular(10.0),
                        )
                    ),
                  ) : _buildIncomingMediaUi(element),
                  new Container(
                    alignment: Alignment.bottomRight,
                    padding: EdgeInsets.all(4.0),
                    child: new Text(/*"12:12AM"*/ "${DateFormat.Hm().format(element.createdAt())}", style: TextStyle(color: Color(0xFF9F9F9F), fontSize: 10.0),),
                  )
                ],
              ),
            ),
          ),
          new SizedBox(width: 70.0,),
        ],
      ),
    );
  }

  void findOrCreateConversation() {

    BackendService.openConversation(
        speakers: [widget.receiver.id, widget.connectedUser.id],
        isGroup: false,
        groupName: null,
        admins: null)
        .then((ConversationResult value){

          if(value != null && value.success && value.data!= null && mounted ){

            setState(() {
              _conversation = value.data;

              _messages = value.data.messages;

            });
          }

    }).catchError((onError){


      print("Error found $onError");

      if(onError is SocketException){
        Toast.show("Vérifiez votre connexion internet !", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      }else if(onError is Response){

        if(onError.statusCode == 400){

          final Map result = jsonDecode(onError.body);
          String message = "Nous rencontrons des problèmes avec le serveur";
          if(result != null && result.containsKey("message")){

            message = result['message'] as String;
          }

          Toast.show(message, context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
        }

      } else{
        final String message = "Nous rencontrons des problèmes avec le serveur";
        Toast.show(message, context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      }


    });

  }

  void sendMessage() {
    if(_conversation != null && messageController.text.trim().isNotEmpty){

      final String content = messageController.text.trim().toString();

      messageController.clear();

      BackendService.sendMessage(
          content: content,
          contentType: Message.TYPE_TEXT,
          senderId: widget.connectedUser.id,
          conversationId: _conversation.id)
      .then((MessageResult value){

        if(value != null && value.success && value.data != null && mounted){

          loadMessages();

        }

      })
      .catchError((onError){

        print("Error found $onError");

        if(onError is SocketException){
          Toast.show("Vérifiez votre connexion internet !", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
        }else if(onError is Response){

          if(onError.statusCode == 400){

            final Map result = jsonDecode(onError.body);
            String message = "Nous rencontrons des problèmes avec le serveur";
            if(result != null && result.containsKey("message")){

              message = result['message'] as String;
            }

            Toast.show(message, context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
          }

        } else{
          final String message = "Nous rencontrons des problèmes avec le serveur";
          Toast.show(message, context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
        }
      });

    }

  }

  void loadMessages() {

   if(_conversation != null){
     BackendService.loadMessagesByConversation(
         conversationId: _conversation.id)
         .then((MessageListResult value){

       if(value != null && value.success && value.data != null && mounted){

         setState(() {
           _messages  = value.data;
         });

       }

     })
         .catchError((onError){

       print("Error found $onError");

       if(onError is SocketException){
         Toast.show("Vérifiez votre connexion internet !", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
       }else if(onError is Response){

         if(onError.statusCode == 400){

           final Map result = jsonDecode(onError.body);
           String message = "Nous rencontrons des problèmes avec le serveur";
           if(result != null && result.containsKey("message")){

             message = result['message'] as String;
           }

           Toast.show(message, context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
         }

       } else{
         final String message = "Nous rencontrons des problèmes avec le serveur";
         Toast.show(message, context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
       }
     });

  }

  }

  Widget _buildOutcomingMediaUi(Message message){

    switch(message.content_type){
      case TYPE_IMAGE:
        return message.content == null? new Container() : new Container(
          child: new Image.network(message.content),
          alignment: Alignment.topLeft,
          padding: EdgeInsets.fromLTRB(18.0, 18.0, 40.0,18.0),
          decoration: BoxDecoration(
              color: Colors.black,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                bottomRight: Radius.circular(10.0),
                bottomLeft:  Radius.circular(10.0),
              )
          ),
        );
      break;
      default :
        return new Container();
    }


  }

  Widget _buildIncomingMediaUi(Message message){

    switch(message.content_type){
      case TYPE_IMAGE:
        return message.content == null? new Container() : new Container(
          child: new Image.network(message.content),
          alignment: Alignment.topLeft,
          padding: EdgeInsets.all(18.0),
          decoration: BoxDecoration(
              color: Color(0xFFE2E2E4),
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(10.0),
                bottomRight: Radius.circular(10.0),
                bottomLeft:  Radius.circular(10.0),
              )
          ),
        );
        break;
      default :
        return new Container();
    }


  }

  Widget emptyView() {

    return new Container(
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          new Center(
            child: new Text("Aucune donnée  disponible", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30.0),),
          ),
          new Center(
            child: new Text("Revenez plutard dans l'espoir d'avoir quelque chose pour vous.", style: TextStyle( fontSize: 16.0),),
          )
        ],
      ),
    );
  }

}


