import 'dart:convert';
import 'dart:io';

import 'package:cinetpay_social_app/ContactScreen.dart';
import 'package:cinetpay_social_app/ConversationWidget.dart';
import 'package:cinetpay_social_app/InboxScreen.dart';
import 'package:cinetpay_social_app/LoginScreen.dart';
import 'package:cinetpay_social_app/models/Conversation.dart';
import 'package:cinetpay_social_app/models/ConversationListResult.dart';
import 'package:cinetpay_social_app/util/LoginManager.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:toast/toast.dart';
import 'models/User.dart';
import 'network/BackendService.dart';

import 'package:firebase_messaging/firebase_messaging.dart';

class HomeScreen extends StatefulWidget {

  final User user;
  const HomeScreen({this.user});

  @override
  _HomeScreenState createState() => _HomeScreenState();

}

class _HomeScreenState extends State<HomeScreen> {

  int _currentPosition = 0;

  List<Conversation> _conversations = [];

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    print("User >>> ${widget.user}");


    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");

      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");

      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");

      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    _firebaseMessaging.getToken().then((String token) {

      print("TOKEN $token");
    });


    loadConversations();

  }

  @override
  Widget build(BuildContext context) {

   if(_conversations == null){
     _conversations = [];
   }
    return new DefaultTabController(
      length: 2,
      child: new Scaffold(
        appBar: new AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          title: new Text("Messages",style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),),
          actions: [
            IconButton(icon: Icon(Icons.refresh), onPressed: (){

              Toast.show("Start loading...", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);

              loadConversations();
            }),
            IconButton(icon: Icon(Icons.exit_to_app), onPressed: (){

              Toast.show("Deconnexion...", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);

              LoginManager.logOut()
              .whenComplete((){

                Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> LoginScreen()));


              });
            })
          ],
          bottom: TabBar(
              tabs: [
                Tab(
                  text: "Messge",
                  //icon: Icon(Icons.message),
                ),
                Tab(
                  text: "Group",
                 // icon: Icon(Icons.people),
                ),
              ]
          ),
        ),
        floatingActionButton: new FloatingActionButton(
          onPressed: ()async{
            print("FAB clicked");

            final User result = await Navigator.push(context, MaterialPageRoute(builder: (context)=> ContactScreen(user: widget.user)));
            if(result != null){

              Navigator.push(context, MaterialPageRoute(builder: (context)=> InboxScreen(receiver: result, connectedUser: widget.user,)));


            }


          },
          child: Icon(Icons.add),
        ),
        body: TabBarView(
            children: [

              new SingleChildScrollView(
                child: new Column(
                  children: _conversations.where((element) => element.lastMessage() != null).map((e) =>    new Container(
                    margin: EdgeInsets.only(top: 10.0),
                    child: new ConversationWidget(
                      element: e,
                      connectedUser: widget.user,
                      onTap: ()async{

                        var result = await Navigator.push(context, MaterialPageRoute(builder: (context)=> InboxScreen(
                          connectedUser: widget.user,
                          receiver: e.getReceiver(widget.user.id),
                          conversation: e,
                        )));

                        loadConversations();

                      },
                    )  ,
                  )).toList(),
                ),
              ),

              new Container(
                color: Colors.blue,
              )

            ]
        ),
        bottomNavigationBar: new BottomNavigationBar(
            currentIndex: _currentPosition,
            onTap: (int position){

              print("Position $position");

              _currentPosition = position;

              print("CurrentPosition $_currentPosition");

              if(mounted){

                setState(() {

                });

              }


            },
            items: [

              BottomNavigationBarItem(
                title: new Text(""),
                icon: Image.asset("images/message_icon.png", color: Color(0xFF91A7C0),),
                activeIcon: Image.asset("images/message_icon.png", color: Colors.black,),
              ),
              BottomNavigationBarItem(
                title: new Text(""),
                icon: Image.asset("images/friends_icon.png", color: Color(0xFF91A7C0),),
                activeIcon: Image.asset("images/friends_icon.png", color: Colors.black,),
              ),
              BottomNavigationBarItem(
                title: new Text(""),
                icon: Image.asset("images/add_box_icon.png", color: Color(0xFF91A7C0),),
                activeIcon: Image.asset("images/add_box_icon.png", color: Colors.black,)
              ),
              BottomNavigationBarItem(
                title: new Text(""),
                icon: Image.asset("images/notifications_icon.png", color: Color(0xFF91A7C0),),
                activeIcon: Image.asset("images/notifications_icon.png", color: Colors.black,),
              ),
              BottomNavigationBarItem(
                title: new Text(""),
                icon: Image.asset("images/profile_icon.png", color: Color(0xFF91A7C0),),
                activeIcon: Image.asset("images/profile_icon.png", color: Colors.black,),
              )

            ]
        ),
      ),
    );
  }

  Widget listTileCinetPay({Widget leftWidget, Widget titleWidget, Widget subtitleWidget,Widget rightWidget}){

    return new Padding(
      padding: EdgeInsets.only(left:16.0, right: 16.0),
      child: new Row(

        children: [
          leftWidget,
          new SizedBox(width: 10.0,),
          new Expanded(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                titleWidget,
                subtitleWidget
              ],
            ),
          ),
          new SizedBox(width: 10.0,),
          rightWidget

        ],
      ),
    );
  }

  void loadConversations() {

    BackendService.loadConversationByUser(
        userId: widget.user.id)
        .then((ConversationListResult value){

      if(value != null && value.success && value.data != null && mounted){

        setState(() {
          _conversations  = value.data;
        });

      }

    })
        .catchError((onError){

      print("Error found $onError");

      if(onError is SocketException){
        Toast.show("Vérifiez votre connexion internet !", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      }else if(onError is Response){

        if(onError.statusCode == 400){

          final Map result = jsonDecode(onError.body);
          String message = "Nous rencontrons des problèmes avec le serveur";
          if(result != null && result.containsKey("message")){

            message = result['message'] as String;
          }

          Toast.show(message, context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
        }

      } else{
        final String message = "Nous rencontrons des problèmes avec le serveur";
        Toast.show(message, context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      }
    });



  }



}




