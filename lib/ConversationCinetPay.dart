import 'package:flutter/material.dart';

class ConversationCinetPay extends StatelessWidget {

  final Widget leftWidget;
  final Widget titleWidget;
  final Widget subtitleWidget;
  final Widget rightWidget;

  ConversationCinetPay({this.leftWidget, this.titleWidget, this.subtitleWidget, this.rightWidget});

  @override
  Widget build(BuildContext context) {
    return new Padding(
      padding: EdgeInsets.only(left:16.0, right: 16.0),
      child: new Row(

        children: [
          leftWidget,
          new SizedBox(width: 10.0,),
          new Expanded(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                titleWidget,
                subtitleWidget
              ],
            ),
          ),
          new SizedBox(width: 10.0,),
          rightWidget

        ],
      ),
    );
  }


}

