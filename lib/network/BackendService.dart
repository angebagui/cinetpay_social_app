import 'dart:convert';

import 'package:cinetpay_social_app/Constants.dart';
import 'package:cinetpay_social_app/models/ContactResult.dart';
import 'package:cinetpay_social_app/models/ConversationListResult.dart';
import 'package:cinetpay_social_app/models/ConversationResult.dart';
import 'package:cinetpay_social_app/models/MessageListResult.dart';
import 'package:cinetpay_social_app/models/MessageResult.dart';
import 'package:cinetpay_social_app/models/UserResult.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
class BackendService{




 static Future<ContactResult> findContacts({int id,    Function showDialog,
   Function hideDialog,})async{

   final String url = "${Constants.API_URL}/contacts/$id"; //https://socialapp.adjemin.com/api/v1/user_register

   showDialog();

   final http.Response response = await http.get(
       url,
       headers: {
         "Accept": "application/json"
       },
   );

   print('=>>>>>>>>>>>>>>>>> register  >>>>>>>>>>>>>>');
   print('=>>>>>>>>>>>>>>>>> REQUEST  >>>>  ${response.request} >>>>>>>>>>>>>');
   print('=>>>>>>>>>>>>>>>>>> RESPONSE STATUS CODE >>>>>>    ${response.statusCode} <<<<<<<<');
   print('=>>>>>>>>>>>>>>>>>> BODY >>>>>>    ${response.body}  <<<<<<');
   print('=>>>>>>>>>>>>>>>>>> register >>>>>>>>>>>>>');

   hideDialog();

   if(response.statusCode == 200){

     final String jsonBodyString = response.body;
     final Map jsonBody = jsonDecode(jsonBodyString);

     if(jsonBody != null){

       return ContactResult.fromJSON(jsonBody);
     }else{

       throw response;
     }

   }else{
     throw response;
   }

 }


 static Future<UserResult> register(
     {
       @required String firstName,
       @required String lastName,
       @required String email,
       @required String password,

       Function showDialog,
       Function hideDialog,
     }
     )async{

    final String url = "${Constants.API_URL}/user_register"; //https://socialapp.adjemin.com/api/v1/user_register

    final Map<String, String> forms = {
      "first_name": firstName,
      "last_name": lastName,
      "email": email,
      "password": password,

    };

    showDialog();

    final http.Response response = await http.post(
        url,
      headers: {
          "Accept": "application/json"
      },
      body: forms
    );

    print('=>>>>>>>>>>>>>>>>> register  >>>>>>>>>>>>>>');
    print('=>>>>>>>>>>>>>>>>> REQUEST  >>>>  ${response.request} >>>>>>>>>>>>>');
    print('=>>>>>>>>>>>>>>>>>> RESPONSE STATUS CODE >>>>>>    ${response.statusCode} <<<<<<<<');
    print('=>>>>>>>>>>>>>>>>>> BODY >>>>>>    ${response.body}  <<<<<<');
    print('=>>>>>>>>>>>>>>>>>> register >>>>>>>>>>>>>');

    hideDialog();

    if(response.statusCode == 200){

      final String jsonBodyString = response.body;
      final Map jsonBody = jsonDecode(jsonBodyString);

      if(jsonBody != null){

        return UserResult.fromJSON(jsonBody);
      }else{

        throw response;
      }

    }else{
      throw response;
    }

  }


 static Future<UserResult> login(
     {
       @required String email,
       @required String password
     }
     )async{

   final String url = "${Constants.API_URL}/user_auth"; //https://socialapp.adjemin.com/api/v1/user_auth

   final Map<String, String> forms = {
     "email": email,
     "password": password,

   };


   final http.Response response = await http.post(
       url,
       headers: {
         "Accept": "application/json"
       },
       body: forms
   );


   print('=>>>>>>>>>>>>>>>>> login  >>>>>>>>>>>>>>');
   print('=>>>>>>>>>>>>>>>>> REQUEST  >>>>  ${response.request} >>>>>>>>>>>>>');
   print('=>>>>>>>>>>>>>>>>>> RESPONSE STATUS CODE >>>>>>    ${response.statusCode} <<<<<<<<');
   print('=>>>>>>>>>>>>>>>>>> BODY >>>>>>    ${response.body}  <<<<<<');
   print('=>>>>>>>>>>>>>>>>>> login >>>>>>>>>>>>>');


   if(response.statusCode == 200){

     final String jsonBodyString = response.body;
     final Map jsonBody = jsonDecode(jsonBodyString);

     if(jsonBody != null){

       return UserResult.fromJSON(jsonBody);
     }else{

       throw response;
     }

   }else{
     throw response;
   }

 }



 static Future<ConversationResult> openConversation(
     {
       @required List<int> speakers,
       @required bool isGroup,
       @required String groupName,
       @required List<int> admins,
     }
     )async{

   final String url = "${Constants.API_URL}/open_conversation"; //https://socialapp.adjemin.com/api/v1/open_conversation

   final Map<String, dynamic> forms = {
     "speakers": speakers,
     "is_group":isGroup,
     "group_name": groupName,
     "admins":admins

   };


   final http.Response response = await http.post(
       url,
       headers: {
         "Accept": "application/json",
         "Content-Type":"application/json"
       },
       body: jsonEncode(forms)
   );


   print('=>>>>>>>>>>>>>>>>> login  >>>>>>>>>>>>>>');
   print('=>>>>>>>>>>>>>>>>> REQUEST  >>>>  ${response.request} >>>>>>>>>>>>>');
   print('=>>>>>>>>>>>>>>>>>> RESPONSE STATUS CODE >>>>>>    ${response.statusCode} <<<<<<<<');
   print('=>>>>>>>>>>>>>>>>>> BODY >>>>>>    ${response.body}  <<<<<<');
   print('=>>>>>>>>>>>>>>>>>> login >>>>>>>>>>>>>');


   if(response.statusCode == 200){

     final String jsonBodyString = response.body;
     final Map jsonBody = jsonDecode(jsonBodyString);

     if(jsonBody != null){

       return ConversationResult.fromJson(jsonBody);
     }else{

       throw response;
     }

   }else{
     throw response;
   }

 }


 static Future<ConversationListResult> loadConversationByUser(
     {
       @required int userId
     }
     )async{

   final String url = "${Constants.API_URL}/conversations/customers/$userId"; //https://socialapp.adjemin.com/api/v1/open_conversation


   final http.Response response = await http.get(
       url,
       headers: {
         "Accept": "application/json"
       },
   );


   print('=>>>>>>>>>>>>>>>>> login  >>>>>>>>>>>>>>');
   print('=>>>>>>>>>>>>>>>>> REQUEST  >>>>  ${response.request} >>>>>>>>>>>>>');
   print('=>>>>>>>>>>>>>>>>>> RESPONSE STATUS CODE >>>>>>    ${response.statusCode} <<<<<<<<');
   print('=>>>>>>>>>>>>>>>>>> BODY >>>>>>    ${response.body}  <<<<<<');
   print('=>>>>>>>>>>>>>>>>>> login >>>>>>>>>>>>>');


   if(response.statusCode == 200){

     final String jsonBodyString = response.body;
     final Map jsonBody = jsonDecode(jsonBodyString);

     if(jsonBody != null){

       return ConversationListResult.fromJson(jsonBody);
     }else{

       throw response;
     }

   }else{
     throw response;
   }

 }




 static Future<MessageResult> sendMessage(
     {
       @required String content,
       @required String contentType,
       @required int senderId,
       @required int conversationId,
     }
     )async{

   final String url = "${Constants.API_URL}/messages"; //https://socialapp.adjemin.com/api/v1/messages

   final Map<String, String> forms = {
     "content": content,
     "content_type": contentType,
     "sender_id": "$senderId",
     "conversation_id": "$conversationId"

   };


   final http.Response response = await http.post(
       url,
       headers: {
         "Accept": "application/json"
       },
       body: forms
   );


   print('=>>>>>>>>>>>>>>>>> login  >>>>>>>>>>>>>>');
   print('=>>>>>>>>>>>>>>>>> REQUEST  >>>>  ${response.request} >>>>>>>>>>>>>');
   print('=>>>>>>>>>>>>>>>>>> RESPONSE STATUS CODE >>>>>>    ${response.statusCode} <<<<<<<<');
   print('=>>>>>>>>>>>>>>>>>> BODY >>>>>>    ${response.body}  <<<<<<');
   print('=>>>>>>>>>>>>>>>>>> login >>>>>>>>>>>>>');


   if(response.statusCode == 200){

     final String jsonBodyString = response.body;
     final Map jsonBody = jsonDecode(jsonBodyString);

     if(jsonBody != null){

       return MessageResult.fromJson(jsonBody);
     }else{

       throw response;
     }

   }else{
     throw response;
   }

 }




 static Future<MessageListResult> loadMessagesByConversation(
     {
       @required int conversationId,
     }
     )async{

   final String url = "${Constants.API_URL}/conversations/messages/$conversationId"; //https://socialapp.adjemin.com/api/v1/conversations/messages/{conversation_id}

   final http.Response response = await http.get(
       url,
       headers: {
         "Accept": "application/json"
       }
   );


   print('=>>>>>>>>>>>>>>>>> login  >>>>>>>>>>>>>>');
   print('=>>>>>>>>>>>>>>>>> REQUEST  >>>>  ${response.request} >>>>>>>>>>>>>');
   print('=>>>>>>>>>>>>>>>>>> RESPONSE STATUS CODE >>>>>>    ${response.statusCode} <<<<<<<<');
   print('=>>>>>>>>>>>>>>>>>> BODY >>>>>>    ${response.body}  <<<<<<');
   print('=>>>>>>>>>>>>>>>>>> login >>>>>>>>>>>>>');


   if(response.statusCode == 200){

     final String jsonBodyString = response.body;
     final Map jsonBody = jsonDecode(jsonBodyString);

     if(jsonBody != null){

       return MessageListResult.fromJson(jsonBody);
     }else{

       throw response;
     }

   }else{
     throw response;
   }

 }

}