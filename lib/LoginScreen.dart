import 'dart:convert';
import 'dart:io';

import 'package:cinetpay_social_app/HomeScreen.dart';
import 'package:cinetpay_social_app/RegisterScreen.dart';
import 'package:cinetpay_social_app/util/LoginManager.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:toast/toast.dart';

import 'models/User.dart';
import 'models/UserResult.dart';
import 'network/BackendService.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  bool _loading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    LoginManager.connectedUser()
    .then((User value){

      if(value != null && mounted){
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context)=> HomeScreen(user:value)));
      }

    })
    .catchError((onError){
      print("Error $onError");
    });

  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();

  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Stack(
        children: [
          new SingleChildScrollView(
            child: new Column(
              children: [

                new Center(
                    child: new Container(
                      width: 150.0,
                      height: 150.0,
                      margin: EdgeInsets.only(top: 50.0),
                      padding: EdgeInsets.all(16.0),
                      decoration: BoxDecoration(
                        //color: Colors.black,
                        border: Border.all(color: Theme.of(context).primaryColor),
                        shape: BoxShape.circle,
                        //image: DecorationImage(image: NetworkImage("https://cinetpay.com/images/logo.png"))

                      ),
                      child: Image.network("https://cinetpay.com/images/logo.png", ),
                    )),

                new SizedBox(height: 10.0,),
                new Center(
                  child: new Text("Connexion", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30.0),),
                ),
                new SizedBox(height: 10.0,),
                new Container(
                  child: new Text("Veuillez vous connecter avec vos identifiants !"),
                ),
                new Container(
                  padding: EdgeInsets.symmetric(horizontal: 16.0),
                  margin: EdgeInsets.all(16.0),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12.0),
                      border: Border.all(color: Theme.of(context).primaryColor)
                  ),
                  child: new TextField(
                    controller: emailController,
                    decoration: InputDecoration(
                        hintText: "Email",
                        border: InputBorder.none
                    ),
                  ),
                ),

                new Container(
                  padding: EdgeInsets.symmetric(horizontal: 16.0),
                  margin: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12.0),
                      border: Border.all(color: Theme.of(context).primaryColor)
                  ),
                  child: new TextField(
                    controller: passwordController,
                    obscureText: true,
                    decoration: InputDecoration(
                        hintText: "Mot de passe",
                        border: InputBorder.none
                    ),
                    keyboardType: TextInputType.text,
                  ),
                ),


                new Container(
                  width: double.infinity,
                  height: 50.0,
                  margin: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
                  child: new RaisedButton(
                    onPressed: (){

                      //var result = await Navigator.push(context, MaterialPageRoute(builder: (context) => new HomeScreen()));

                      //print('Result $result');
                      loginUser();

                    },
                    color: Theme.of(context).accentColor,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                    child: new Text("Se Connecter", style: TextStyle(color: Colors.white),),
                  ),
                ),

                new Container(
                  width: double.infinity,
                  height: 50.0,
                  margin: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
                  child: new RaisedButton(
                    onPressed: (){

                      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> RegisterScreen()));

                    },
                    color: Theme.of(context).primaryColor,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                    child: new Text("Créer compte", style: TextStyle(color: Colors.white),),
                  ),
                ),

                new Container(
                    child: InkWell(
                      onTap: (){

                        print('Mot de passe Oublié clicked');
                      },
                      child: new Text("Mot de passe Oublié", style: TextStyle(decoration: TextDecoration.underline),),
                    )
                )

              ],
            ),
          ),
          new Offstage(
            offstage: _loading,
            child: progressWidget(),
          )
        ],
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void didUpdateWidget(LoginScreen oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);

  }

  void loginUser() {


    String email = emailController.text.trim().toString();
    String password = passwordController.text.trim().toString();

    print("EMAIL $email");
    print("PASSWORD $password");

    if(emailController.text.trim().toString().isEmpty){

      //Display Toast
      Toast.show("Email obligatoire", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      return;
    }

    if(passwordController.text.trim().toString().isEmpty){

      //Display Toast
      Toast.show("Mot de passe obligatoire", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      return;
    }


    showProgress();

    BackendService.login(
        email: emailController.text.trim().toString(),
        password: passwordController.text.trim().toString())
        .then((UserResult value){

      hideProgress();

      print("VALUE $value");


      if(value.success){

        LoginManager.storeUser(value.data)
        .catchError((onError){
          print("Error $onError");
        })
        .whenComplete((){

          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context)=> HomeScreen(user:value.data)));

        });


      }else{
        Toast.show(value.message, context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      }




    })
        .catchError((onError){

      hideProgress();

      print("Error found $onError");

      if(onError is SocketException){
        Toast.show("Vérifiez votre connexion internet !", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      }else if(onError is Response){

        if(onError.statusCode == 400){

          final Map result = jsonDecode(onError.body);
          String message = "Nous rencontrons des problèmes avec le serveur";
          if(result != null && result.containsKey("message")){

            message = result['message'] as String;
          }

          Toast.show(message, context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
        }

      } else{
        final String message = "Nous rencontrons des problèmes avec le serveur";
        Toast.show(message, context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      }

    });


  }

  Widget progressWidget(){
    return new Stack(
      children: [

        Container(
          color: Colors.white,
        ),

        Center(child: CircularProgressIndicator())
      ],
    ) ;
  }

  void showProgress(){

    if(mounted){
      setState(() {
        _loading = false;
      });
    }


  }

  void hideProgress(){

    if(mounted){
      setState(() {
        _loading = true;
      });
    }
  }
}
