import 'package:cinetpay_social_app/models/User.dart';
import 'package:flutter/material.dart';

class ContactWidget extends StatelessWidget {

  final User element;

  final Function onTap;

  const ContactWidget(this.element, this.onTap);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListTile(
        onTap: onTap,
        leading: new Container(
          width: 60.0,
          height: 60.0,
          decoration: BoxDecoration(

            shape: BoxShape.circle,
            image: DecorationImage(image: NetworkImage(element.photo))
          ),
        ),
        title: new Text(element == null? "": ("${element.first_name}  ${element.last_name}")),
        subtitle: new Text(element == null? "": (element.email == null? "":"${element.email}")),
        trailing: new Icon(Icons.arrow_forward_ios),

      ),
    );
  }
}
